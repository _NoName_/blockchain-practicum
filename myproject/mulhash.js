function mulHashFormula(x, a, W, M) {
    return Math.floor((a*x % W) / (W/M));
}

function mulHashBitwise(x, a, m) {
    return (a*x) >> (16-m);
}

module.exports = {
    mulHashFormula: mulHashFormula,
    mulHashBitwise: mulHashBitwise
};