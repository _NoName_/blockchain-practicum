const MulHash = require('./mulhash.js');

test('count multiplicative hashing function', () => {
    const hash1 = MulHash.mulHashBitwise(1,1512519,1);
    const hash2 = MulHash.mulHashBitwise(1,1512517,1);
    const hash3 = MulHash.mulHashBitwise(1,15129115,1);
    const hash4 = MulHash.mulHashBitwise(1,15129117,1);
    expect(hash1).toBe(46);
    expect(hash2).toBe(46);
    expect(hash3).toBe(461);
    expect(hash4).toBe(461);
});