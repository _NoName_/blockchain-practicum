# Prerequisites

Update your system packages with `sudo apt update`

Install `nodejs` following this link:
https://joshtronic.com/2018/05/08/how-to-install-nodejs-10-on-ubuntu-1804-lts/
or alternative link
https://github.com/nodejs/help/wiki/Installation

Install git with `sudo apt install git`

Install Webstorm following these instructions:
https://www.jetbrains.com/help/webstorm/install-and-set-up-product.html

Check all versions:
* `node -v` => `^10`
* `npm -v` => `^6`
* `git --version` => `^2`
