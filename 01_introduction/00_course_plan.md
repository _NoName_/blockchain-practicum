# Course objectives
Topics covered in this course, rather in chronological order
* Using industry standard toolset through creating blockchain building blocks. 
    * Toolset includes:
        * `nodejs`, `npm`
        * Git version control system, Gitlab, git workflow (gitflow)
        * Test Driven Development, `jest` test framework
        * Webstorm IDE
    * Ingredients include:
        * hashing algorithms
        * proofOfWork, alternatives
* Using the above toolset and solutions for creating a JavaScript (nodejs) powered Blockchain
* Exploring the Ethereum Blockchain in practice:
    * setting up a private node (geth or parity)
    * Solidity language, compilers, Remix, ganache, zeppelin
    * deploying Smart Contracts on test networks like Kovan / Ropsten
    * using web3, Metamask, hardware wallet
* Exploring other blockchains, like HyperLedger