# Chrome V8
V8 is an open-source JavaScript engine, known from Chrome browser.
* compiles JavaScript directly to native machine code
* performs optimizations
* intended to be used both in a browser and as a standalone engine
