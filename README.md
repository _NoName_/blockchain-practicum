# Blockchain - Practicum

This is the main repo of blockchain-practicum course

## https://gitlab.com/tomasz.rozmus/blockchain-practicum

## Classes
1: Introduction
 * [Course draft](01_introduction/00_course_plan.md)
 * [Environment preparation](01_introduction/01_environment.md)
 * [Git version control system](01_introduction/01_git.md)
 * [Node.js](01_introduction/02_nodejs.md)
 * [NPM project](01_introduction/03_npm.md)
 * [Test Driven Development](01_introduction/06_tdd.md)
 